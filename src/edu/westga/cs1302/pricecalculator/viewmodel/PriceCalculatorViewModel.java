package edu.westga.cs1302.pricecalculator.viewmodel;

import java.text.NumberFormat;

import edu.westga.cs1302.pricecalculator.model.PriceCalculator;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * The Class PriceCalculatorViewModel.
 * @author Omen
 */
public class PriceCalculatorViewModel {
	
	/** The unit price property. */
	private final DoubleProperty unitPriceProperty;
	
	/** The quanity property. */
	private final IntegerProperty quanityProperty;
	
	/** The max discount property. */
	private final IntegerProperty maxDiscountProperty;
	
	/** The display property. */
	private final StringProperty displayProperty;
	
	/** The price calculator. */
	private PriceCalculator priceCalculator;
	
	/** The Constant TAB. */
	private static final String TAB = "			";
	
	/**
	 * Instantiates a new price calculator view model.
	 */
	public PriceCalculatorViewModel() {
		this.unitPriceProperty = new SimpleDoubleProperty();
		this.quanityProperty = new SimpleIntegerProperty();
		this.maxDiscountProperty = new SimpleIntegerProperty();
		this.displayProperty = new SimpleStringProperty();
		
		this.priceCalculator = new PriceCalculator();
		

	}

	/**
	 * Gets the unit price property.
	 *
	 * @return the unit price property
	 */
	public DoubleProperty getUnitPriceProperty() {
		return this.unitPriceProperty;
	}

	/**
	 * Gets the quanity property.
	 *
	 * @return the quanity property
	 */
	public IntegerProperty getQuanityProperty() {
		return this.quanityProperty;
	}

	/**
	 * Gets the max discount property.
	 *
	 * @return the max discount property
	 */
	public IntegerProperty getMaxDiscountProperty() {
		return this.maxDiscountProperty;
	}

	/**
	 * Gets the display property.
	 *
	 * @return the display property
	 */
	public StringProperty getDisplayProperty() {
		return this.displayProperty;
	}
	
	/**
	 * Calculate total.
	 *
	 * @return the double
	 */
	public double calculateTotal() {
		this.priceCalculator.setQuantity(this.getQuanityProperty().get());
		this.priceCalculator.setUnitPrice(this.getUnitPriceProperty().get());
		return this.priceCalculator.calculateRegularTotal();

	}
	
	/**
	 * Calculate saving.
	 *
	 * @return the double
	 */
	public double calculateSaving() {
		return this.priceCalculator.calculateTotalSavings(this.getMaxDiscountProperty().get());
	}
	
	/**
	 * Clear.
	 */
	public void clear() {
		this.displayProperty.set("");
	}
	
	/**
	 * Discount table.
	 *
	 * @return the string
	 */
	public String discountTable() {
		NumberFormat formater = NumberFormat.getCurrencyInstance();
		String output = "unit price	" + "        quantity" + "        	       total payment" + System.lineSeparator();

		try {
			output += this.getUnitPriceProperty().get() + TAB + this.getQuanityProperty().get() + TAB + "       " + this.calculateTotal()
		    	+ System.lineSeparator() + System.lineSeparator() + "Discount%" + "	Price w/discount" + "	savings" + System.lineSeparator();
		
			for (int i = 0; i <= this.getMaxDiscountProperty().get(); i += 5) {
				output += i + "%" + TAB + formater.format(this.priceCalculator.calculateDiscountedTotal(i)) + TAB + formater.format(this.priceCalculator.calculateTotalSavings(i))
					+ System.lineSeparator();
			
			}
		
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("nope");
			alert.setContentText("you thought that was a valid input?");
			alert.showAndWait();
		}
		return output;
	}
}

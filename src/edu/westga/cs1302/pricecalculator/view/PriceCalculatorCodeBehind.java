package edu.westga.cs1302.pricecalculator.view;

import edu.westga.cs1302.pricecalculator.viewmodel.PriceCalculatorViewModel;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

/**
 * The Class priceCalculatorCodeBehind.
 * @author Omen
 */
public class PriceCalculatorCodeBehind {
    
    /** The unit price text field. */
    @FXML
    private TextField unitPriceTextField;

    /** The quanity text field. */
    @FXML
    private TextField quanityTextField;

    /** The discount text field. */
    @FXML
    private TextField discountTextField;

    /** The generate price button. */
    @FXML
    private Button generatePriceButton;
   
    /** The output txt area. */
    @FXML
    private TextArea outputTxtArea;
    
    /** The view model. */
    private PriceCalculatorViewModel viewModel;


    /**
     * Instantiates a new price calculator code behind.
     */
    public PriceCalculatorCodeBehind() {
    	this.viewModel = new PriceCalculatorViewModel();
    }
    
	/**
	 * Initialize.
	 */
	@FXML
	void initialize() {
		this.outputTxtArea.setEditable(false);
		
		this.unitPriceTextField.textProperty().bindBidirectional(this.viewModel.getUnitPriceProperty(), new NumberStringConverter());
		this.quanityTextField.textProperty().bindBidirectional(this.viewModel.getQuanityProperty(), new NumberStringConverter());
		this.discountTextField.textProperty().bindBidirectional(this.viewModel.getMaxDiscountProperty(), new NumberStringConverter());
		this.viewModel.getDisplayProperty().bind(this.outputTxtArea.textProperty());
	}
	
	/**
	 * Handle generate price.
	 */
	@FXML
	private void handleGeneratePrice() {
		this.outputTxtArea.clear();
		this.outputTxtArea.appendText(this.viewModel.discountTable());
	}
}
